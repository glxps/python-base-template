# python-base-template

## How the project have been installed and freezed
```bash
python3 -m venv .env
source .env/bin/activate
python3 -m pip install Flask
python3 -m pip freeze > requirements.txt
```

## How to run the project
```bash
flask run
```
You can run the project with `FLASK_ENV=development` env variable with turn on the debuging mode.
```bash
FLASK_ENV=development flask run
```
Or more pernanentely exporting the variable
```bash
export FLASK_ENV=development
flask
```